import { HttpClientModule } from '@angular/common/http';
import { ErrorHandler, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { Base64ToGallery } from '@ionic-native/base64-to-gallery';
import { Clipboard } from '@ionic-native/clipboard';
import { NativeStorage } from '@ionic-native/native-storage';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { HomePage } from '../pages/home/home';
import { CanvasApiProvider } from '../providers/canvas-api/canvas-api';
import { CanvasEditorProvider } from '../providers/canvas-editor/canvas-editor';
import { HistoryImgComponent } from './../components/history-img/history-img';
import { XCanvasComponent } from './../components/x-canvas/x-canvas';
import { AuthPage } from './../pages/auth/auth';
import { CanvasPage } from './../pages/canvas/canvas';
import { CreateCanvasPage } from './../pages/create-canvas/create-canvas';
import { LoginPage } from './../pages/login/login';
import { PencilMenuPage } from './../pages/pencil-menu/pencil-menu';
import { SignupPage } from './../pages/signup/signup';
import { UserListPage } from './../pages/user-list/user-list';
import { MyApp } from './app.component';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    AuthPage,
    LoginPage,
    SignupPage,
    CanvasPage,
    UserListPage,
    PencilMenuPage,
    XCanvasComponent,
    CreateCanvasPage,
    HistoryImgComponent
  ],
  imports: [BrowserModule, HttpClientModule, IonicModule.forRoot(MyApp)],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    AuthPage,
    CanvasPage,
    UserListPage,
    PencilMenuPage,
    CreateCanvasPage,
    HistoryImgComponent
  ],
  providers: [
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    Clipboard,
    StatusBar,
    SplashScreen,
    NativeStorage,
    Base64ToGallery,
    CanvasApiProvider,
    CanvasEditorProvider
  ]
})
export class AppModule {}
