import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { CanvasEditorProvider } from './../../providers/canvas-editor/canvas-editor';

@Component({
  selector: 'page-pencil-menu',
  templateUrl: 'pencil-menu.html'
})
export class PencilMenuPage {
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public canvasEditor: CanvasEditorProvider
  ) {}
}
