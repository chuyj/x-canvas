import { Component, EventEmitter, Output } from '@angular/core';
import { ToastController, ViewController } from 'ionic-angular';
import { CanvasApiProvider } from './../../providers/canvas-api/canvas-api';
import { HomePage } from './../home/home';
import { SignupPage } from './../signup/signup';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  @Output('modechange') modechage = new EventEmitter<string>();

  account: string = '';
  password: string = '';
  signupPage = SignupPage;
  homePage = HomePage;
  constructor(
    private canvasApi: CanvasApiProvider,
    public viewCtrl: ViewController,
    public toastCtrl: ToastController
  ) {}

  login() {
    this.canvasApi.login(this.account, this.password).subscribe(isLogin => {
      if (isLogin) {
        const toast = this.toastCtrl.create({
          message: 'Login Success',
          duration: 2000,
          position: 'top'
        });
        toast.present();
        this.viewCtrl.dismiss();
      } else {
        const toast = this.toastCtrl.create({
          message: 'Login Failed',
          duration: 2000,
          position: 'top'
        });
        toast.present();
      }
    });
  }

  signup() {
    this.modechage.emit('signup');
  }
}
