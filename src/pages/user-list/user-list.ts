import { Component } from '@angular/core';
import { ToastController } from 'ionic-angular';
import { CanvasAction } from './../../providers/canvas-api/canvas-api';
import { CanvasEditorProvider } from './../../providers/canvas-editor/canvas-editor';

@Component({
  selector: 'page-user-list',
  templateUrl: 'user-list.html'
})
export class UserListPage {
  get users() {
    return this.canvasEditor.users;
  }

  get painter() {
    return this.canvasEditor.painter;
  }

  constructor(
    private toastCtrl: ToastController,
    private canvasEditor: CanvasEditorProvider
  ) {}

  setPainter(user: { name: string; account: string }) {
    if (this.canvasEditor.isOwner()) {
      const action: CanvasAction = {
        type: 'setPainter',
        payload: { painter: user.account }
      };
      this.canvasEditor.broadcastAction(action);
    } else {
      const toast = this.toastCtrl.create({
        message: 'You are not allowed to change the painter',
        duration: 2000
      });
      toast.present();
    }
  }
}
