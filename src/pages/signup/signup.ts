import { Component, EventEmitter, Output } from '@angular/core';
import {
  NavController,
  NavParams,
  ToastController,
  ViewController
} from 'ionic-angular';
import { CanvasApiProvider } from './../../providers/canvas-api/canvas-api';

@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html'
})
export class SignupPage {
  @Output() modechange = new EventEmitter<string>();
  name: string = '';
  account: string = '';
  password: string = '';
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    private canvasApi: CanvasApiProvider,
    public toastCtrl: ToastController
  ) {}
  signup() {
    this.canvasApi
      .signup(this.name, this.account, this.password)
      .subscribe(success => {
        if (success) {
          const toast = this.toastCtrl.create({
            message: 'Signup Success',
            duration: 2000,
            position: 'top'
          });
          toast.present();
          this.viewCtrl.dismiss();
        } else {
          const toast = this.toastCtrl.create({
            message: 'Signup Failed',
            duration: 2000,
            position: 'top'
          });
          toast.present();
        }
      });
  }
  login() {
    this.modechange.emit('login');
  }
}
