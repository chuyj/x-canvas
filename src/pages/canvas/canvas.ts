import { Component, OnInit, ViewChild } from '@angular/core';
import { Base64ToGallery } from '@ionic-native/base64-to-gallery';
import { Clipboard } from '@ionic-native/clipboard';
import {
  ActionSheetController,
  AlertController,
  Navbar,
  NavController,
  NavParams,
  PopoverController,
  ToastController
} from 'ionic-angular';
import { UserListPage } from '../user-list/user-list';
import { CanvasAction } from './../../providers/canvas-api/canvas-api';
import { CanvasEditorProvider } from './../../providers/canvas-editor/canvas-editor';
import { PencilMenuPage } from './../pencil-menu/pencil-menu';

@Component({
  selector: 'page-canvas',
  templateUrl: 'canvas.html'
})
export class CanvasPage implements OnInit {
  error = false;
  canvasId: number;

  debugData: any;

  @ViewChild(Navbar) navbar: Navbar;

  constructor(
    private clipboard: Clipboard,
    private navParams: NavParams,
    private navCtrl: NavController,
    private alertCtrl: AlertController,
    private toastCtrl: ToastController,
    private b64ToGallery: Base64ToGallery,
    private popoverCtrl: PopoverController,
    public canvasEditor: CanvasEditorProvider,
    private actionSheetCtrl: ActionSheetController
  ) {
    this.canvasId = this.navParams.get('canvasId');
  }

  ngOnInit() {
    this.navbar.backButtonClick = () => {
      this.navCtrl.popToRoot();
    };
    this.canvasEditor.onAction.subscribe(action => {
      this.handleAction(action);
    });
  }

  handleAction(action: CanvasAction) {
    switch (action.type) {
      case 'error':
        const alert = this.alertCtrl.create({
          title: 'Error',
          subTitle: action.payload.message,
          buttons: [
            {
              text: 'OK',
              handler: () => {
                this.error = true;
                this.navCtrl.popToRoot();
              }
            }
          ],
          enableBackdropDismiss: false
        });
        alert.present();
    }
  }

  ionViewWillEnter() {
    this.navCtrl.swipeBackEnabled = false;
  }

  ionViewDidLeave() {
    this.navCtrl.swipeBackEnabled = true;
  }

  ionViewWillLeave() {
    if (!this.error) {
      this.canvasEditor.oldCanvas.set(this.canvasId, {
        title: this.canvasEditor.title,
        dataURL: this.canvasEditor.combined.toDataURL()
      });
    }
  }

  rename() {
    if (!this.canvasEditor.isOwner()) {
      return;
    }
    const alert = this.alertCtrl.create({
      title: 'Rename',
      inputs: [
        {
          name: 'title',
          value: this.canvasEditor.title
        }
      ],
      buttons: [
        {
          text: 'Cancel'
        },
        {
          text: 'Done',
          handler: data => {
            const action: CanvasAction = {
              type: 'setTitle',
              payload: { title: data.title }
            };
            this.canvasEditor.broadcastAction(action);
          }
        }
      ]
    });
    alert.present();
  }

  share() {
    const actionSheet = this.actionSheetCtrl.create({
      title: 'Share',
      buttons: [
        {
          text: `Copy Canvas ID (${this.canvasId})`,
          icon: 'clipboard',
          handler: () => {
            this.onCopy();
          }
        },
        {
          text: 'Save To Camera Roll',
          icon: 'image',
          handler: () => {
            this.saveImage();
          }
        },
        {
          text: 'Cancel',
          role: 'cancel'
        }
      ]
    });
    actionSheet.present();
  }

  saveImage() {
    const data = this.canvasEditor.combined.toDataURL();
    this.b64ToGallery.base64ToGallery(data).then(() => {
      const toast = this.toastCtrl.create({
        message: 'The canvas has been saved to camera roll.',
        duration: 2000
      });
      toast.present();
    });
  }

  menu() {
    const actionSheet = this.actionSheetCtrl.create({
      title: 'Menu',
      buttons: [
        {
          text: 'Clear Canvas',
          role: 'destructive',
          handler: () => {
            const action = { type: 'clearCanvas' };
            this.canvasEditor.broadcastAction(action);
          }
        },
        {
          text: 'Cancel',
          role: 'cancel'
        }
      ]
    });
    actionSheet.present();
  }

  onCopy() {
    this.clipboard.copy(`${this.canvasId}`);
    const toast = this.toastCtrl.create({
      message: 'ID has been copied to clipboard',
      duration: 2000
    });
    toast.present();
  }

  shiftHistory(shift: number) {
    if (this.canvasEditor.isPainter()) {
      this.canvasEditor.historyCount = Math.min(
        Math.max(0, this.canvasEditor.historyCount + shift),
        this.canvasEditor.queuedPrimitive.length
      );
      const action: CanvasAction = {
        type: 'setHistory',
        payload: { history: this.canvasEditor.historyCount }
      };
      this.canvasEditor.broadcastAction(action);
    } else {
      const toast = this.toastCtrl.create({
        message: 'You are not allowed to paint right now',
        duration: 2000
      });
      toast.present();
    }
  }

  togglePencilMenu(event: Event) {
    if (this.canvasEditor.activeTool === 'pen') {
      const menu = this.popoverCtrl.create(PencilMenuPage);
      menu.present({ ev: event });
    }
    this.canvasEditor.activeTool = 'pen';
  }

  toggleUserMenu() {
    const menu = this.popoverCtrl.create(UserListPage, undefined, {
      cssClass: 'custom-popover'
    });
    menu.present();
  }
}
