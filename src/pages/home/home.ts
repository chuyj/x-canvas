import { CanvasEditorProvider } from './../../providers/canvas-editor/canvas-editor';
import { Component } from '@angular/core';
import {
  AlertController,
  LoadingController,
  ModalController,
  NavController
} from 'ionic-angular';
import { CanvasApiProvider } from './../../providers/canvas-api/canvas-api';
import { AuthPage } from './../auth/auth';
import { CanvasPage } from './../canvas/canvas';
import { CreateCanvasPage } from './../create-canvas/create-canvas';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  canvasId: string;

  constructor(
    private navCtrl: NavController,
    private modalCtrl: ModalController,
    private alertCtrl: AlertController,
    private canvasApi: CanvasApiProvider,
    private loadingCtrl: LoadingController,
    public canvasEditor: CanvasEditorProvider
  ) {}

  get oldCanvas() {
    return Array.from(this.canvasEditor.oldCanvas.keys());
  }

  ionViewDidEnter() {
    if (!this.canvasApi.authenticated) {
      const modal = this.modalCtrl.create(AuthPage, undefined, {
        showBackdrop: false
      });
      modal.present();
    }
  }

  async joinCanvas() {
    this.navCtrl.push(CanvasPage, { canvasId: this.canvasId });
  }

  async createCanvas() {
    const loading = this.loadingCtrl.create({
      content: 'Creating the Canvas'
    });

    await loading.present();

    this.canvasApi.create().subscribe(
      async response => {
        await loading.dismiss();
        if (response.type === 'success') {
          this.navCtrl.push(CreateCanvasPage, {
            canvasId: response.payload['canvasId']
          });
        }
      },
      () => {
        const warning = this.alertCtrl.create({
          title: 'Failed to connect to server',
          buttons: ['OK']
        });
        warning.present();
      }
    );
  }
}
