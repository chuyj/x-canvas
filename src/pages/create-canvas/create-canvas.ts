import { Component } from '@angular/core';
import { Clipboard } from '@ionic-native/clipboard';
import { NavController, NavParams, ToastController } from 'ionic-angular';
import { CanvasPage } from './../canvas/canvas';

@Component({
  selector: 'page-create-canvas',
  templateUrl: 'create-canvas.html'
})
export class CreateCanvasPage {
  canShowToast = true;
  canvasId: string;
  constructor(
    private navParams: NavParams,
    private clipboard: Clipboard,
    private navCtrl: NavController,
    private toastCtrl: ToastController
  ) {
    this.canvasId = this.navParams.get('canvasId');
  }

  onCopy() {
    this.clipboard.copy(`${this.canvasId}`);
    if (this.canShowToast) {
      this.canShowToast = false;
      const toast = this.toastCtrl.create({
        message: 'ID has been copied to clipboard',
        duration: 2000,
        cssClass: 'doodle'
      });
      toast.onDidDismiss(() => {
        this.canShowToast = true;
      });
      toast.present();
    }
  }

  onStart() {
    this.navCtrl.push(CanvasPage, { canvasId: this.canvasId });
  }
}
