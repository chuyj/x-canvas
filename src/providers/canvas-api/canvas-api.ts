import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { WebSocketSubject } from 'rxjs/observable/dom/WebSocketSubject';
import { map, tap } from 'rxjs/operators';
import { CanvasAction } from './canvas-api';

const HOST: string = '172.16.0.177';
const PORT: number = 8888;

export interface CanvasAction {
  type: string;
  payload?: any;
}

@Injectable()
export class CanvasApiProvider {
  name: string;
  account: string;
  password: string;

  constructor(public http: HttpClient) {}

  create() {
    return this.http.post<CanvasAction>(`http://${HOST}:${PORT}/canvas`, {
      account: this.account,
      password: this.password
    });
  }

  websocket(canvasId: string) {
    return WebSocketSubject.create<CanvasAction>(
      `ws://${HOST}:${PORT}/canvas/${canvasId}`
    );
  }

  get authenticated(): boolean {
    return !!this.account;
  }

  login(account: string, password: string) {
    const payload = { account, password };
    return this.http
      .post<CanvasAction>(`http://${HOST}:${PORT}/login`, payload)
      .pipe(
        tap(action => {
          if (action['type'] === 'success') {
            this.account = account;
            this.password = password;
            this.name = action.payload.name;
          }
        }),
        map(action => {
          return action['type'] === 'success';
        })
      );
  }

  signup(name: string, account: string, password: string) {
    const payload = { name, account, password };
    return this.http
      .post<CanvasAction>(`http://${HOST}:${PORT}/signup`, payload)
      .pipe(
        tap(action => {
          if (action.type === 'success') {
            this.account = account;
            this.password = password;
            this.name = name;
          }
        }),
        map(action => {
          return action.type === 'success';
        })
      );
  }
}
