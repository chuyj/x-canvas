import { Injectable } from '@angular/core';
import { ToastController } from 'ionic-angular';
import { WebSocketSubject } from 'rxjs/observable/dom/WebSocketSubject';
import { Subject } from 'rxjs/Subject';
import { Subscription } from 'rxjs/Subscription';
import { CanvasAction, CanvasApiProvider } from './../canvas-api/canvas-api';

export interface Vertex {
  x: number;
  y: number;
}

export interface Primitive {
  type: string;
  color?: string;
  width?: number;
  vertices?: Vertex[];
}

export interface CanvasData {
  title: string;
  dataURL: string;
}

@Injectable()
export class CanvasEditorProvider {
  readonly WIDTH = 1200;
  readonly HEIGHT = 1600;
  readonly PEN_COLORS = [
    '#e74c3c',
    '#f1c40f',
    '#2ecc71',
    '#3498db',
    '#9b59b6',
    '#2c3e50'
  ];
  readonly PEN_WIDTHS = [10, 30, 80, 120];

  oldCanvas = new Map<number, CanvasData>();

  owner: string;
  painter: string;
  title: string;

  users: { account: string; name: string }[] = [];
  username = new Map<string, string>();

  private websocket: WebSocketSubject<CanvasAction>;
  private subscription: Subscription;

  onAction = new Subject<CanvasAction>();

  penColorIdx = this.PEN_COLORS.length - 1;
  penWidthIdx = 0;
  activeTool = 'pen';
  historyCount = 0;
  queuedPrimitive: Primitive[] = [];

  combined: HTMLCanvasElement;

  readonly HISTORY_LIMIT = 5;

  constructor(
    private toastCtrl: ToastController,
    private canvasApi: CanvasApiProvider
  ) {
    this.combined = document.createElement('canvas');
    this.combined.width = this.WIDTH;
    this.combined.height = this.HEIGHT;
  }

  selectTool(tool: string) {
    this.activeTool = tool;
  }

  setPenWidth(widthIndex: number) {
    this.penWidthIdx = widthIndex;
  }

  get penWidth(): number {
    return this.PEN_WIDTHS[this.penWidthIdx];
  }

  setPenColor(colorIndex: number) {
    this.penColorIdx = colorIndex;
  }

  get penColor() {
    return this.PEN_COLORS[this.penColorIdx];
  }

  connect(canvasId: string) {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    this.websocket = this.canvasApi.websocket(canvasId);

    this.subscription = this.websocket.subscribe(action => {
      this.handleAction(action);
      this.onAction.next(action);
    });

    this.websocket.socket.onopen = () => {
      const action: CanvasAction = {
        type: 'auth',
        payload: {
          account: this.canvasApi.account,
          password: this.canvasApi.password
        }
      };
      this.sendAction(action);
    };
  }

  disconnect() {
    if (this.subscription) {
      this.subscription.unsubscribe();
      this.subscription = null;
      this.websocket = null;
    }
  }

  sendAction(action: CanvasAction) {
    if (this.websocket) {
      this.websocket.socket.send(JSON.stringify(action));
    }
  }

  broadcastAction(action: CanvasAction) {
    this.onAction.next(action);
    this.sendAction(action);
  }

  handleAction(action: CanvasAction) {
    switch (action.type) {
      case 'setUserList':
        this.users = action.payload.users;
        this.username.clear();
        this.users.forEach(user => {
          this.username.set(user.account, user.name);
        });
        break;

      case 'setPrivilege':
        this.owner = action.payload.owner;
        this.painter = action.payload.painter;
        const name = this.username.get(this.painter);
        if (name) {
          const toast = this.toastCtrl.create({
            message: `The painter has been changed to ${name}.`,
            duration: 2000
          });
          toast.present();
        }
        break;
    }
  }

  isPainter() {
    return this.canvasApi.account === this.painter;
  }

  isOwner() {
    return this.canvasApi.account === this.owner;
  }
}
