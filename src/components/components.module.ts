import { NgModule } from '@angular/core';
import { HistoryImgComponent } from './history-img/history-img';
import { XCanvasComponent } from './x-canvas/x-canvas';
@NgModule({
  declarations: [HistoryImgComponent, XCanvasComponent],
  imports: [],
  exports: [HistoryImgComponent, XCanvasComponent]
})
export class ComponentsModule {}
