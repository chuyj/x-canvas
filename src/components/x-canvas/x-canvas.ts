import {
  Component,
  ElementRef,
  Input,
  OnDestroy,
  OnInit,
  Renderer2,
  ViewChild
} from '@angular/core';
import { ToastController } from 'ionic-angular';
import { WebSocketSubject } from 'rxjs/observable/dom/WebSocketSubject';
import { Subscription } from 'rxjs/Subscription';
import {
  CanvasAction,
  CanvasApiProvider
} from './../../providers/canvas-api/canvas-api';
import {
  CanvasEditorProvider,
  Primitive,
  Vertex
} from './../../providers/canvas-editor/canvas-editor';

@Component({
  selector: 'x-canvas',
  templateUrl: 'x-canvas.html'
})
export class XCanvasComponent implements OnInit, OnDestroy {
  canShowToast: boolean = true;
  intervalID: number;

  ws: WebSocketSubject<CanvasAction>;
  subscription: Subscription;

  @Input() canvasId: string;

  @ViewChild('canvasBg') canvasBgRef: ElementRef;
  @ViewChild('canvasDiv') canvasDivRef: ElementRef;
  @ViewChild('upperCanvas') upperCanvasRef: ElementRef;
  @ViewChild('lowerCanvas') lowerCanvasRef: ElementRef;
  @ViewChild('historyCanvas') historyCanvasRef: ElementRef;

  upperCanvas: HTMLCanvasElement;
  lowerCanvas: HTMLCanvasElement;
  historyCanvas: HTMLCanvasElement;

  drawingPrimitive: Primitive | null;

  private touchId: number | null = null;

  constructor(
    private renderer: Renderer2,
    private toastCtrl: ToastController,
    public canvasApi: CanvasApiProvider,
    public canvasEditor: CanvasEditorProvider
  ) {}

  get viewHeight() {
    const canvasBgDiv: HTMLDivElement = this.canvasBgRef.nativeElement;
    return canvasBgDiv.getBoundingClientRect().height;
  }

  get viewWidth() {
    const canvasBgDiv: HTMLDivElement = this.canvasBgRef.nativeElement;
    return canvasBgDiv.getBoundingClientRect().width;
  }

  get offsetX() {
    const canvasBgDiv: HTMLDivElement = this.canvasBgRef.nativeElement;
    return canvasBgDiv.getBoundingClientRect().left;
  }

  get offsetY() {
    const canvasBgDiv: HTMLDivElement = this.canvasBgRef.nativeElement;
    return canvasBgDiv.getBoundingClientRect().top;
  }

  ngOnInit() {
    this.upperCanvas = this.upperCanvasRef.nativeElement;
    this.lowerCanvas = this.lowerCanvasRef.nativeElement;
    this.historyCanvas = this.historyCanvasRef.nativeElement;

    this.subscription = this.canvasEditor.onAction.subscribe(action => {
      this.handleAction(action);
    });

    this.canvasEditor.connect(this.canvasId);

    this.intervalID = setInterval(() => {
      this.resize();
    }, 250);
  }

  resize() {
    const canvasDiv: HTMLDivElement = this.canvasDivRef.nativeElement;
    const { width, height } = canvasDiv.getBoundingClientRect();
    const rate = Math.min(width / 3, height / 4);
    const viewWidth = 3 * rate;
    const viewHeight = 4 * rate;
    const canvasBgDiv: HTMLDivElement = this.canvasBgRef.nativeElement;
    this.renderer.setStyle(canvasBgDiv, 'width', viewWidth + 'px');
    this.renderer.setStyle(canvasBgDiv, 'height', viewHeight + 'px');
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
    this.canvasEditor.disconnect();
    clearInterval(this.intervalID);
  }

  activeTouch(event: TouchEvent): Touch | null {
    switch (event.type) {
      case 'touchstart':
        if (this.touchId === null) {
          return event.changedTouches[0];
        }
        return null;
      case 'touchmove':
      case 'touchend':
        if (this.touchId !== null) {
          for (let i = 0; i < event.changedTouches.length; ++i) {
            const touch = event.changedTouches[i];
            if (touch.identifier === this.touchId) {
              return touch;
            }
          }
        }
        return null;
      default:
        return null;
    }
  }

  canvasCoord(touch: Touch): Vertex {
    const viewX = touch.pageX - this.offsetX;
    const viewY = touch.pageY - this.offsetY;
    return {
      x: Math.floor((viewX * this.canvasEditor.WIDTH) / this.viewWidth + 0.5),
      y: Math.floor((viewY * this.canvasEditor.HEIGHT) / this.viewHeight + 0.5)
    };
  }

  onTouchStart(event: TouchEvent) {
    if (!this.canvasEditor.isPainter()) {
      if (this.canShowToast) {
        this.canShowToast = false;
        const toast = this.toastCtrl.create({
          message: 'You are not allowed to paint right now',
          duration: 2000
        });
        toast.present();
        toast.onDidDismiss(() => {
          this.canShowToast = true;
        });
      }
      return;
    }

    const touch = this.activeTouch(event);
    if (touch === null) {
      return;
    }

    this.touchId = touch.identifier;
    const coord = this.canvasCoord(touch);

    const action = {
      type: 'startPrimitive',
      payload: { x: coord.x, y: coord.y } as any
    };

    switch (this.canvasEditor.activeTool) {
      case 'pen':
        action.payload.type = 'path';
        action.payload.width = this.canvasEditor.penWidth;
        action.payload.color = this.canvasEditor.penColor;
        break;

      case 'eraser':
        action.payload.type = 'eraser';
        action.payload.width = 50;
        action.payload.color = '#ffffff';
        break;
    }

    this.canvasEditor.broadcastAction(action);
  }

  onTouchMove(event: TouchEvent) {
    if (!this.canvasEditor.isPainter()) {
      return;
    }
    const touch = this.activeTouch(event);
    if (touch === null) {
      return;
    }
    const coord = this.canvasCoord(touch);
    const action = {
      type: 'addVertex',
      payload: { x: coord.x, y: coord.y }
    };

    this.canvasEditor.broadcastAction(action);
  }

  onTouchEnd(event: TouchEvent) {
    if (!this.canvasEditor.isPainter()) {
      return;
    }
    const touch = this.activeTouch(event);
    if (touch === null) {
      return;
    }

    const action = {
      type: 'endPrimitive'
    };

    this.touchId = null;

    this.canvasEditor.broadcastAction(action);
  }

  handleAction(action: CanvasAction) {
    switch (action.type) {
      case 'startPrimitive':
        const primitive = action.payload;
        switch (primitive.type) {
          case 'path':
          case 'eraser':
            this.drawingPrimitive = {
              type: primitive.type,
              color: primitive.color,
              width: primitive.width,
              vertices: [{ x: primitive.x, y: primitive.y }]
            };
            break;
        }
        break;

      case 'addVertex':
        switch (this.drawingPrimitive.type) {
          case 'path':
          case 'eraser':
            this.drawingPrimitive.vertices.push(action.payload);
            break;
        }
        this.redrawUpper();
        break;

      case 'clearCanvas':
        this.drawingPrimitive = { type: 'clear' };
      case 'endPrimitive':
        this.canvasEditor.queuedPrimitive = this.canvasEditor.queuedPrimitive.slice(
          0,
          this.canvasEditor.historyCount
        );
        this.canvasEditor.queuedPrimitive.push(this.drawingPrimitive);
        this.canvasEditor.historyCount = this.canvasEditor.queuedPrimitive.length;

        while (
          this.canvasEditor.historyCount > this.canvasEditor.HISTORY_LIMIT
        ) {
          const primitive = this.canvasEditor.queuedPrimitive.shift();
          this.drawPrimitive(this.historyCanvas, primitive);
          --this.canvasEditor.historyCount;
        }

        this.drawPrimitive(this.lowerCanvas, this.drawingPrimitive);
        this.drawingPrimitive = null;
        this.redrawUpper();
        this.redrawCombined();
        break;

      case 'loadCanvas':
        this.canvasEditor.title = action.payload.title;
        this.drawingPrimitive = action.payload.drawingPrimitive;
        this.canvasEditor.queuedPrimitive = action.payload.queuedPrimitive;
        this.canvasEditor.historyCount = action.payload.historyCount;
        this.redrawLower();
        if (action.payload.historyImage) {
          const img = this.renderer.createElement('img') as HTMLImageElement;
          img.onload = () => {
            const ctx = this.historyCanvas.getContext(
              '2d'
            ) as CanvasRenderingContext2D;
            ctx.drawImage(img, 0, 0);
            this.redrawCombined();
          };
          img.src = action.payload.historyImage;
        } else {
          this.redrawCombined();
        }
        break;

      case 'setHistory':
        this.canvasEditor.historyCount = action.payload.history;
        this.redrawLower();
        this.redrawCombined();
        break;

      case 'setTitle':
        this.canvasEditor.title = action.payload.title;
        break;
    }
  }

  redrawCombined() {
    const ctx = this.canvasEditor.combined.getContext(
      '2d'
    ) as CanvasRenderingContext2D;
    ctx.fillStyle = 'white';
    ctx.fillRect(0, 0, ctx.canvas.width, ctx.canvas.height);
    ctx.drawImage(this.historyCanvas, 0, 0);
    ctx.drawImage(this.lowerCanvas, 0, 0);
    ctx.drawImage(this.upperCanvas, 0, 0);
  }

  drawPrimitive(canvas: HTMLCanvasElement, primitive: Primitive) {
    if (!primitive) {
      return;
    }
    const ctx = canvas.getContext('2d') as CanvasRenderingContext2D;
    switch (primitive.type) {
      case 'path':
      case 'eraser':
        const vertices = primitive.vertices as Vertex[];
        const firstV = vertices[0];
        ctx.strokeStyle = primitive.color;
        ctx.lineWidth = primitive.width;
        ctx.lineCap = 'round';
        ctx.beginPath();
        ctx.moveTo(firstV.x, firstV.y);
        for (let i = 1; i < vertices.length - 1; ++i) {
          const v1 = vertices[i];
          const v2 = vertices[i + 1];
          const mid: Vertex = {
            x: Math.floor((v1.x + v2.x) / 2),
            y: Math.floor((v1.y + v2.y) / 2)
          };
          ctx.quadraticCurveTo(v1.x, v1.y, mid.x, mid.y);
        }
        const lastV = vertices[vertices.length - 1];
        ctx.lineTo(lastV.x, lastV.y);
        ctx.lineTo(lastV.x + 0.01, lastV.y + 0.01);
        ctx.stroke();
        break;

      case 'clear':
        ctx.fillStyle = 'white';
        ctx.fillRect(0, 0, ctx.canvas.width, ctx.canvas.height);
        break;
    }
  }

  redrawUpper() {
    const ctx: CanvasRenderingContext2D = this.upperCanvas.getContext('2d');
    ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
    if (this.drawingPrimitive == null) {
      return;
    }
    this.drawPrimitive(this.upperCanvas, this.drawingPrimitive);
  }

  redrawLower() {
    const ctx = this.lowerCanvas.getContext('2d') as CanvasRenderingContext2D;
    ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
    for (let i = 0; i < this.canvasEditor.historyCount; ++i) {
      const primitive = this.canvasEditor.queuedPrimitive[i];
      this.drawPrimitive(this.lowerCanvas, primitive);
    }
  }
}
