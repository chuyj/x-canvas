import { CanvasPage } from './../../pages/canvas/canvas';
import { NavController } from 'ionic-angular';
import { Component, Input } from '@angular/core';
import { CanvasEditorProvider } from './../../providers/canvas-editor/canvas-editor';

@Component({
  selector: 'history-img',
  templateUrl: 'history-img.html'
})
export class HistoryImgComponent {
  @Input() canvasId: number;

  constructor(
    private canvasEditor: CanvasEditorProvider,
    private navCtrl: NavController
  ) {}

  get title() {
    const canvasData = this.canvasEditor.oldCanvas.get(this.canvasId);
    if (canvasData) {
      return canvasData.title;
    }
    return '';
  }

  get dataURL() {
    const canvasData = this.canvasEditor.oldCanvas.get(this.canvasId);
    if (canvasData) {
      return canvasData.dataURL;
    }
    return '';
  }

  joinCanvas() {
    this.navCtrl.push(CanvasPage, { canvasId: this.canvasId });
  }
}
